from tqdm import tqdm
import numpy as np
from models import Book, Library

#
#
def read_input():
  libraries = []

  books_n, libraries_n, days = map(lambda x: int(x), input().split())
  book_scores = np.array(list(map(lambda x: int(x), input().split())))
  book_scores_sorted_i = np.argsort(book_scores)[::-1]

  # set shared vars
  Library.all_book_scores = book_scores
  Library.days = days

  # init libraries
  for i in tqdm(range(libraries_n)):
    books_n, signup_time, scans_per_day = map(lambda x: int(x), input().split())
    book_ids = np.array(list(map(lambda x: int(x), input().split())))
    libraries.append(Library(i, signup_time, scans_per_day, book_scores_sorted_i[np.in1d(book_scores_sorted_i, book_ids)]))

  return (book_scores, book_scores_sorted_i, libraries, days)

#
#
def print_output(libraries_used):
  libraries_used = list(filter(lambda l: len(l._book_ids_to_scan) > 0, libraries_used))

  print(len(libraries_used))
  for library in libraries_used:
    print('{} {}'.format(library.id, len(library._book_ids_to_scan)))
    print(' '.join(list(map(lambda x: str(x), library._book_ids_to_scan))))

def libraries_split_i(sorted_libraries, days):
  sorted_libraries_used = sorted_libraries
  sorted_libraries_rem = []
  d = 0
  for i in range(len(sorted_libraries)):
    library = sorted_libraries[i]

    d += library.signup_time
    library._day_signup_completed = d
    library._available_slots = (days-d) * library.scans_per_day
    library._book_ids_to_scan = []

    if d > days:
      return int(i)

def create_book_index(sorted_libraries_used, book_scores_sorted_i):
  books_to_lib_index = {}
  books_ids_used = set()
  for library in sorted_libraries_used:
    for id in library.book_ids:
      if id in books_to_lib_index:
        books_to_lib_index[id].append(library.id)
      else:
        books_to_lib_index[id] = [library.id]
    books_ids_used |= set(library.book_ids)

  books_ids_used = np.array(list(books_ids_used))
  books_ids_used_sorted = book_scores_sorted_i[np.in1d(book_scores_sorted_i, books_ids_used)]

  return (books_ids_used_sorted, books_to_lib_index)

def main():
  book_scores, book_scores_sorted_i, libraries, days = read_input()

  dd = int(days * 2)
  Library.days = dd
  sorted_libraries = sorted(libraries, key=lambda l: l._score, reverse=True)

  si = libraries_split_i(sorted_libraries, days)
  sorted_libraries_used = sorted_libraries[:si]
  sorted_libraries_rem = sorted_libraries[si:]

  books_ids_used_sorted, books_to_lib_index = create_book_index(sorted_libraries_used, book_scores_sorted_i)

  for id in tqdm(books_ids_used_sorted):
    for lid in reversed(books_to_lib_index[id]):
      if libraries[lid].scan(id):
        break

  # pick libraries by best real score
  dd = days
  Library.days = days
  libraries_final = sorted(sorted_libraries_used, key=lambda l: l.real_score(), reverse=True)

  si = libraries_split_i(libraries_final, days)
  sorted_libraries_used = libraries_final[:si]
  sorted_libraries_rem = libraries_final[si:]

  books_ids_used_sorted, books_to_lib_index = create_book_index(sorted_libraries_used, book_scores_sorted_i)

  for id in tqdm(books_ids_used_sorted):
    for lid in reversed(books_to_lib_index[id]):
      if libraries[lid].scan(id):
        break

  print_output(sorted_libraries_used)

if __name__ == '__main__':
  main()
