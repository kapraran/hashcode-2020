import numpy as np

class Book:
  def __init__(self, id, score):
    self.id = id
    self.score = score

class Library:
  all_book_scores = None
  days = None

  def __init__(self, id, signup_time, scans_per_day, book_ids):
    self.id = id
    self.signup_time = signup_time
    self.scans_per_day = scans_per_day
    self.book_ids = book_ids

    self._books = None
    self._score = None
    self._day_signup_completed = None
    self._available_slots = None
    self._book_ids_to_scan = None

    self.calc_score()

  def calc_score(self, days=None):
    if days is None:
      book_ids = self.book_ids
    else:
      slots = days * self.scans_per_day
      book_ids = self.book_ids[:slots]

    w = 0.6
    books_score = np.sum(self.all_book_scores[book_ids])
    # self._score = books_score/(self.signup_time + w * len(self.book_ids)/self.scans_per_day)
    self._score = books_score/(self.signup_time)

  def real_score(self):
    # print(self._book_ids_to_scan)
    books_score = np.sum(self.all_book_scores[np.array(self._book_ids_to_scan)])
    return books_score

  def scan(self, bid):
    if self._available_slots > len(self._book_ids_to_scan):
      self._book_ids_to_scan.append(bid)
      return True
    return False
    