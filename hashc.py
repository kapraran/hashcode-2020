#!/usr/bin/python

import sys
import os
import json
import zipfile
import argparse
import datetime
import time
import re
from shutil import copyfile
from subprocess import call

DEF_CONFIG_FILENAME = "hashc.conf"
DEF_LATEST_CONFIG_FILENAME = ".latest"
DEF_LATEST_DIRNAME = "000-latest"
DEF_LOG_FILENAME = ".hashc-log"

class Config:
  def __init__(self, filename=DEF_CONFIG_FILENAME):
    self.__data = {}
    self.__filename = filename
    self.load()

  def load(self):
    """
    Loads the configuration from the config file
    """
    if not os.path.isfile(self.__filename):
      return

    with open(self.__filename, "r") as file:
      self.__data = {**self.__data, **json.load(file)}

  def save(self):
    """
    Saves the current config data into the config file 
    """
    with open(self.__filename, "w") as file:
      file.write(json.dumps(self.__data, indent=2))

  def has(self, key):
    return key in self.__data

  def set(self, key, value):
    self.__data[key] = value

  def get(self, key, dval=None):
    return self.__data[key] if self.has(key) else dval

  def __getitem__(self, key):
    return self.get(key)

  def all(self):
    return self.__data


def parse_args(config):
  parser = argparse.ArgumentParser(description="hashc - simple hashcode toolkit")

  parser.add_argument("-i", action="store", default="*")
  parser.add_argument("-m", action="store", default=None)
  parser.add_argument("-x", action="store_true", default=False)
  parser.add_argument("-z", action="store_true", default=False)
  parser.add_argument("-g", action="store_true", default=False)

  return parser.parse_args()

def zip_src(zip_filepath, config):
  zipf = zipfile.ZipFile(zip_filepath, "w", zipfile.ZIP_DEFLATED)

  # walk along the path and add all files to the zip
  for root, dirs, filenames in os.walk(config.get("source_path")):
    for filename in filenames:
      zipf.write(os.path.join(root, filename))

  zipf.close()

def resolve_input_filenames(arg, config):
  input_path = config.get("input_path")

  # filter file list
  all_filenames = [f for f in os.listdir(input_path) if os.path.isfile(os.path.join(input_path, f))]
  input_filenames = [f for f in all_filenames if re.match(config.get("input_test"), f)]

  # all
  if arg == "*":
    return input_filenames

  # range
  elif re.match(r"(\d+)\.\.(\d+)", arg):
    start, end = re.match(r"(\d+)\.\.(\d+)", arg).groups()
    return input_filenames[int(start):int(end) + 1]

  # specific files by index
  elif re.match(r"((\d+)\,)*\d+", arg):
    indexes = map(lambda x: int(x), arg.split(","))
    return [input_filenames[i] for i in indexes]

  # specific files by name prefix
  elif re.match(r"((.+)\,)*.+", arg):
    prefixes = arg.split(",")
    prefixes_lens = set([len(p) for p in prefixes])
    filtered = []
    for l in prefixes_lens:
      for f in input_filenames:
        prefix = f[:l]
        if prefix in prefixes:
          filtered.append(f)

    return sorted(list(set(filtered)))

  # else raise error
  else:
    raise ValueError("Invalid input argument value")

def prepare_output_dirs(now_str, config):
  latest_path = os.path.join(config.get("output_path"), DEF_LATEST_DIRNAME)
  build_output_path = os.path.join(config.get("output_path"), now_str)

  # check latest dir
  if not os.path.isdir(latest_path):
    os.makedirs(latest_path)

  # create new build dir
  os.makedirs(build_output_path)
  return build_output_path

def update_latest(key, value, config, latest_config):
  # delete old file
  if latest_config.has(key):
    filename = latest_config.get(key)
    filepath = os.path.join(config.get("output_path"), DEF_LATEST_DIRNAME, filename)
    os.remove(filepath)

  # update config
  latest_config.set(key, value)
  latest_config.save()

def git_commit(version):
  log_stream = open(DEF_LOG_FILENAME, "a+")

  call(["git", "add", "--all"], stdout=log_stream, stderr=log_stream)
  call(["git", "commit", "-m", "[hashc] Auto commit ver:{0}".format(version)], stdout=log_stream, stderr=log_stream)

def print_welcome():
  print(" _               _          ")
  print("| |__   __ _ ___| |__   ___ ")
  print("| '_ \\ / _` / __| '_ \\ / __|")
  print("| | | | (_| \\__ \\ | | | (__ ")
  print("|_| |_|\\__,_|___/_| |_|\\___|\n")

def main():
  t_main_start = time.time()

  # welcome message
  print_welcome()

  # init config files
  config = Config()
  latest_config = Config(os.path.join(config.get("output_path"), DEF_LATEST_DIRNAME, DEF_LATEST_CONFIG_FILENAME))

  # parse cmd arguments
  args = parse_args(config)
  input_filenames = resolve_input_filenames(args.i, config)

  if len(input_filenames) < 1:
    raise Exception("There are no input files selected")

  # set now string
  today = datetime.datetime.today()
  now_str = today.strftime("%Y%m%d%H%M%S")

  # create output dirs
  output_dir = prepare_output_dirs(now_str, config)

  # create zip
  if args.z:
    zip_filename = config.get("zip_format").format(team=config.get("team"), year=config.get("year"), round=config.get("round"), now=now_str)
    zip_filepath = os.path.join(output_dir, zip_filename)
    print("[Z] Zipping source code")
    zip_src(zip_filepath, config)

    # copy zip to latest dir
    zip_latest_filepath = os.path.join(config.get("output_path"), DEF_LATEST_DIRNAME, zip_filename)
    copyfile(zip_filepath, zip_latest_filepath)
    update_latest("zip", zip_filename, config, latest_config)

  # commit changes
  if args.g:
    print("[G] Commiting changes into git repository")
    git_commit(now_str)

  # execute selected input
  if args.x:
    for f in input_filenames:
      input_filepath = os.path.join(config.get("input_path"), f)
      output_filename = config.get("output_format").format(input=f, team=config.get("team"), year=config.get("year"), round=config.get("round"), now=now_str)
      output_filepath = os.path.join(output_dir, output_filename)

      # setup io and call script
      in_stream = open(input_filepath)
      out_stream = open(output_filepath, "w")
      main_script_filename = args.m if not args.m is None else config.get("main_script")
      main_script_path = os.path.join(config.get("source_path"), main_script_filename)

      if not os.path.isfile(main_script_path):
        raise Exception("Cannot find main script \"{0}\"".format(main_script_filename))

      call_args = config.get("call_args")
      call_args = [arg.format(main_script=main_script_path) for arg in call_args]

      print("[X] Executing script for input \"{0}\"".format(f))
      t_start = time.time()
      ret_code = call(call_args, stdin=in_stream, stdout=out_stream)

      res_str = "successfully" if ret_code == 0 else "with errors"
      print("    -> Completed {0} in {1:.2f}s".format(res_str, time.time() - t_start))

      # copy output to latest dir
      output_latest_filepath = os.path.join(config.get("output_path"), DEF_LATEST_DIRNAME, output_filename)
      copyfile(output_filepath, output_latest_filepath)
      update_latest(f, output_filename, config, latest_config)

  print("\n[~] Total execution time {0:.2f}s".format(time.time() - t_main_start))
  print("[~] You can check the resulting files in \"{0}\" directory".format(output_dir))

if __name__ == "__main__":
  main()
